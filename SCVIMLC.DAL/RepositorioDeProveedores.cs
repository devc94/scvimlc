﻿using SCVIMLC.COMMON.Entidades;
using SCVIMLC.COMMON.Interfaces;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCVIMLC.DAL
{
    class RepositorioDeProveedores : IRepositorio<Proveedor>
    {
        List<Proveedor> GetProveedors()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString.Value))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SELECT id, nombre, direccion, telefono, email FROM proveedores;", conn))
                    {
                        cmd.CommandType = CommandType.Text;
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                var proveedores = new List<Proveedor>();
                                while (dr.Read())
                                {
                                    var p = new Proveedor
                                    {
                                        ID = dr.GetInt32(0),
                                        Nombre = dr.GetString(1),
                                        Direccion = dr.GetString(2),
                                        Telefono = dr.GetString(3),
                                        Email = dr.GetString(4)
                                    };

                                    proveedores.Add(p);
                                }
                                return proveedores;
                            }
                            else
                            {
                                return new List<Proveedor>();
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                return new List<Proveedor>();
            }
        }

        public List<Proveedor> Read => throw new NotImplementedException();

        public bool Create(Proveedor entidad)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString.Value))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("spInsertarProveedor", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@nombre", entidad.Nombre);
                        cmd.Parameters.AddWithValue("@direccion", entidad.Direccion);
                        cmd.Parameters.AddWithValue("@telefono", entidad.Telefono);
                        cmd.Parameters.AddWithValue("@email", entidad.Email);

                        return (cmd.ExecuteNonQuery() > 0) ? true : false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString.Value))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("spEliminarProveedor", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@id", id);

                        return (cmd.ExecuteNonQuery() > 0) ? true : false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(Proveedor entidad)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString.Value))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("spModificarProveedor", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@id", entidad.ID);
                        cmd.Parameters.AddWithValue("@nombre", entidad.Nombre);
                        cmd.Parameters.AddWithValue("@direccion", entidad.Direccion);
                        cmd.Parameters.AddWithValue("@telefono", entidad.Telefono);
                        cmd.Parameters.AddWithValue("@email", entidad.Email);

                        return (cmd.ExecuteNonQuery() > 0) ? true : false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}

﻿using SCVIMLC.COMMON.Entidades;
using SCVIMLC.COMMON.Interfaces;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCVIMLC.DAL
{
    public class RepositorioDeCategorias : IRepositorio<Categoria>
    {
        List<Categoria> GetCategorias() 
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString.Value))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("SELECT id, categoria FROM categorias;", conn))
                    {
                        cmd.CommandType = CommandType.Text;
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                var categorias = new List<Categoria>();
                                while (dr.Read())
                                {
                                    var c = new Categoria
                                    {
                                        ID = dr.GetInt32(0),
                                        NombreCategoria = dr.GetString(1)
                                    };

                                    categorias.Add(c);
                                }
                                return categorias;
                            } else
                            {
                                return new List<Categoria>();
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                return new List<Categoria>();
            }
        }

        public List<Categoria> Read => GetCategorias();

        public bool Create(Categoria entidad)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString.Value))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("spInsertarCategoria", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@categoria", entidad.NombreCategoria);

                        return (cmd.ExecuteNonQuery() > 0) ? true : false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString.Value))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("spEliminarCategoria", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@id", id);

                        return (cmd.ExecuteNonQuery() > 0) ? true : false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(Categoria entidad)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString.Value))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("spModificarCategoria", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@id", entidad.ID);
                        cmd.Parameters.AddWithValue("@categoria", entidad.NombreCategoria);

                        return (cmd.ExecuteNonQuery() > 0) ? true : false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}

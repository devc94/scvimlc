﻿using SCVIMLC.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCVIMLC.DAL
{
    public static class RConnectionString
    {
        public static string GetConnectionString()
        {
            return ConnectionString.Value;
        }

        public static void SetConnectionString(string connectionString)
        {
            ConnectionString.Value = connectionString;
        }
    }
}

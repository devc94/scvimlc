﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCVIMLC.COMMON.Entidades
{
    public class Rol : Base
    {
        public string NombreRol { get; set; }
        public string Permisos { get; set; }
    }
}

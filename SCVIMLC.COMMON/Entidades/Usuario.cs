﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCVIMLC.COMMON.Entidades
{
    public class Usuario : Base
    {
        public string Nombre { get; set; }
        public string NombreUsuario { get; set; }
        public string Passwd { get; set; }
        public Rol Rol { get; set; }
        public int Estado { get; set; }
    }
}

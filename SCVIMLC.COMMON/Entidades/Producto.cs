﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCVIMLC.COMMON.Entidades
{
    public class Producto : Base
    {
        public string CodigoProducto { get; set; }
        public string NombreProducto { get; set; }
        public Categoria Categoria { get; set; }
        public Proveedor Proveedor { get; set; }
        public decimal Costo { get; set; }
        public decimal Precio { get; set; }
    }
}
